<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\FavoriteController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

//publicas
Route::post('/register', [AuthController::class, 'register']);
Route::post('/login', [AuthController::class, 'login']);

//privadas requieren de un token
Route::middleware('auth:api')->group(function() {
    Route::put('/update-user', [AuthController::class, 'update']);
    Route::get('/user', [AuthController::class, 'me']);
    Route::post('/add-favorite', [FavoriteController::class, 'add']);
    Route::get('/favorite/{id}', [FavoriteController::class, 'index']);
    Route::get('/favorites', [FavoriteController::class, 'show']);
    Route::delete('/remove-favorite/{id}', [FavoriteController::class, 'remove']);
    Route::post('/logout', [AuthController::class, 'logout']);
});