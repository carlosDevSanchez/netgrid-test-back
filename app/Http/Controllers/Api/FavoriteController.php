<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Favorite;

class FavoriteController extends Controller
{
    public function __construct(){}

    public function show(){
        $user_auth = Auth()->user();
        $favorites = Favorite::where('id_usuario', $user_auth->id)->get();

        return response()->json([
            'meta' => [
                'code' => 200,
                'status' => 'success'
            ],
            'data' => [
                'favorites' => $favorites
            ],
        ]);
    }

    public function index($id){
        $user_auth = Auth()->user();
        $favorite = Favorite::where('id_usuario', $user_auth->id)->where('ref_api', $id)->first();

        return response()->json([
            'meta' => [
                'code' => 200,
                'status' => 'success'
            ],
            'data' => [
                'favorite' => $favorite
            ],
        ]);
    }

    public function add(Request $request){
        $this->validate($request, [
            'id' => 'required|integer',
        ]);

        $user_auth = Auth()->user();
        $favorite = Favorite::create([
            'id_usuario' => $user_auth->id,
            'ref_api' => $request['id']
        ]);

        return response()->json([
            'meta' => [
                'code' => 200,
                'status' => 'success',
                'message' => 'Favorite created successfully!',
            ],
            'data' => [
                'favorite' => $favorite
            ],
        ]);
    }

    public function remove($id){
        $user_auth = Auth()->user();
        Favorite::where('id_usuario', $user_auth->id)->where('ref_api', $id)->delete();

        return response()->json([
            'meta' => [
                'code' => 200,
                'status' => 'success',
                'message' => 'Favorite remove successfully!',
            ]
        ]);
    }
}
